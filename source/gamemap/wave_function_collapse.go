package gamemap

import (
	"log"
	spr "simulator/source/sprite"
	t "simulator/source/tile"
	u "simulator/source/utils"
)

type Tuple struct {
	X int32
	Y int32
}

type WaveFunctionCell struct {
	PossibleTiles map[int]bool
	Modified      bool
}

func WaveFunctionCollapse(
	gamemap *Map,
	floorSprites []*spr.Sprite,
	wallSprites []*spr.Sprite,
) {
	// 100 possibilidades para cada tile
	mapCells := make(map[Tuple]*WaveFunctionCell, gamemap.Cols*gamemap.Rows)
	{ //CLEAN MAP
		gamemap.Cells = make([][]*Cell, gamemap.Cols)
		for i := int32(0); i < gamemap.Rows; i++ {
			gamemap.Cells[i] = make([]*Cell, gamemap.Rows)
			for j := int32(0); j < gamemap.Cols; j++ {
				key := Tuple{X: i, Y: j}
				cell := &WaveFunctionCell{
					PossibleTiles: make(map[int]bool, 100),
				}
				for k := 0; k < 100; k++ {
					cell.PossibleTiles[k] = true
				}
				mapCells[key] = cell
			}
		}
	}

	{
		floodFill(0, 0, gamemap, floorSprites, wallSprites, mapCells, true)
	}
}

var DIRECTIONS = []*Tuple{
	//{X: -1, Y: -1},
	{X: +0, Y: -1},
	//{X: +1, Y: -1},
	{X: -1, Y: +0},
	//center
	{X: +1, Y: +0},
	//{X: -1, Y: +1},
	{X: +0, Y: +1},
	//{X: +1, Y: +1},
}

func floodFill(
	i int32, j int32,
	gamemap *Map,
	floorSprites []*spr.Sprite,
	wallSprites []*spr.Sprite,
	mapCells map[Tuple]*WaveFunctionCell,
	first bool,
) {
	if outOfBounds(i, j, gamemap.Rows, gamemap.Cols) {
		return
	}
	if gamemap.Cells[i][j] != nil {
		return
	}
	key := Tuple{X: i, Y: j}
	cell, ok := mapCells[key]
	if !ok {
		log.Fatalln("Error, mapCells wasn't populated correctly")
	}
	if first {
		tile, variant := getRandomTile(cell.PossibleTiles)
		switch val := tile.(type) {
		case t.WallTileEnum:
			gamemap.Cells[i][j].Tile = t.NewWallTile(
				wallSprites,
				val,
				false,
				u.Vector2[int32]{X: i, Y: j},
			)
		case t.FloorTileShadowEnum:
			gamemap.Cells[i][j].Tile = t.NewFloorTile(
				floorSprites,
				val,
				variant,
				u.Vector2[int32]{X: i, Y: j},
			)
		}
	} else {
		if len(cell.PossibleTiles) > 1 {

			cell.Modified = true
		} else {
			for k := range cell.PossibleTiles {
				tile, variant := getTile(k)
				switch val := tile.(type) {
				case t.WallTileEnum:
					gamemap.Cells[i][j].Tile = t.NewWallTile(
						wallSprites,
						val,
						false,
						u.Vector2[int32]{X: i, Y: j},
					)
				case t.FloorTileShadowEnum:
					gamemap.Cells[i][j].Tile = t.NewFloorTile(
						floorSprites,
						val,
						variant,
						u.Vector2[int32]{X: i, Y: j},
					)
				}
			}

		}
	}

	for k := 0; k < len(DIRECTIONS); k++ {
		dirX := i + DIRECTIONS[k].X
		dirY := j + DIRECTIONS[k].Y
		floodFill(dirX, dirY, gamemap, floorSprites, wallSprites, mapCells, false)
	}
}

func outOfBounds(x, y, maxX, maxY int32) bool {
	return !(x >= 0 && x < maxX) || !(y >= 0 && y < maxY)
}

func getRandomTile(possibleTiles map[int]bool) (tileEnum any, variant int) {
	return nil, 0
}
func getTile(possibleTile int) (tileEnum any, variant int) {
	return nil, 0
}

type CellTilesEnum uint8

const (
	F1_NO_SHADOW CellTilesEnum = iota
	F1_INNER_CORNER
	F1_BOTTOM
	F1_OUTER_CORNER
	F1_LEFT
	F1_DIAGONAL_BOTTOM
	F1_DIAGONAL_LEFT
	F1_DIAGONAL_CORNER
	F2_NO_SHADOW
	F2_INNER_CORNER
	F2_BOTTOM
	F2_OUTER_CORNER
	F2_LEFT
	F2_DIAGONAL_BOTTOM
	F2_DIAGONAL_LEFT
	F2_DIAGONAL_CORNER
	F3_NO_SHADOW
	F3_INNER_CORNER
	F3_BOTTOM
	F3_OUTER_CORNER
	F3_LEFT
	F3_DIAGONAL_BOTTOM
	F3_DIAGONAL_LEFT
	F3_DIAGONAL_CORNER
	F4_NO_SHADOW
	F4_INNER_CORNER
	F4_BOTTOM
	F4_OUTER_CORNER
	F4_LEFT
	F4_DIAGONAL_BOTTOM
	F4_DIAGONAL_LEFT
	F4_DIAGONAL_CORNER
	W_CORNER_TOP_LEFT
	W_LEFT
	W_CORNER_BOTTOM_LEFT
	W_HORIZONTAL_LEFT
	W_TOP
	W_ENCLOSED
	W_BOTTOM
	W_HORIZONTAL
	W_CORNER_TOP_RIGHT
	W_RIGHT
	W_CORNER_BOTTOM_RIGHT
	W_HORIZONTAL_RIGHT
	W_VERTICAL_TOP
	W_VERTICAL
	W_VERTICAL_BOTTOM
	W_PILLAR
	W_VERTICAL_TOP_SHADOW
	W_VERTICAL_SHADOW
	W_VERTICAL_BOTTOM_SHADOW
	W_PILLAR_SHADOW
	W_LINE_RIGHT_BOTTOM
	W_LINE_TOP_RIGHT_BOTTOM
	W_LINE_TOP_RIGHT
	W_HORIZONTAL_LEFT_SHADOW
	W_LINE_LEFT_BOTTOM_RIGHT
	W_LINE_CROSS
	W_LINE_LEFT_TOP_RIGHT
	W_HORIZONTAL_SHADOW
	W_LINE_LEFT_BOTTOM
	W_LINE_TOP_LEFT_BOTTOM
	W_LINE_LEFT_TOP
	W_HORIZONTAL_RIGHT_SHADOW
	W_p
	W_OPEN_LEFT_TOP_RIGHT_BOTTOM
	W_b
	W_p_SHADOW
	W_OPEN_TOP_LEFT_BOTTOM_RIGHT
	W_OPEN_BOTTOM_LEFT_TOP_RIGHT
	W_LINE_TOP_RIGHT_BOTTOM_SHADOW
	W_q
	W_OPEN_RIGHT_LEFT_TOP_BOTTOM
	W_d
	W_b_SHADOW
	W_LINE_CROSS_BOTTOM_RIGHT
	W_LINE_CROSS_TOP_RIGHT
	W_CORNER_TOP_LEFT_SHADOW
	W_LEFT_SHADOW
	W_LINE_CROSS_BOTTOM_LEFT
	W_LINE_CROSS_TOP_LEFT
	W_CORNER_BOTTOM_LEFT_SHADOW
	W_SPECIAL_TOP_LEFT_SHADOW
	W_SPECIAL_BOTTOM_LEFT
	W_SPECIAL_TOP_LEFT
	W_BOTTOM_SHADOW
	W_SPECIAL_TOP_RIGHT_SHADOW
	W_SPECIAL_BOTTOM_RIGHT
	W_SPECIAL_TOP_RIGHT
	W_CORNER_BOTTOM_RIGHT_SHADOW
	W_LINE_LEFT_TOP_RIGHT_SHADOW
	W_INNER_CORNER_BOTTOM_RIGHT
	W_INNER_CORNER_TOP_RIGHT
	W_LINE_RIGHT_BOTTOM_SHADOW
	W_LINE_TOP_RIGHT_SHADOW
	W_INNER_CORNER_BOTTOM_LEFT
	W_INNER_CORNER_TOP_LEFT
	W_LINE_LEFT_TOP_SHADOW
	W_SLASH
	W_BACKSLASH
)
