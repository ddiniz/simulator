package gamemap

import (
	"bufio"
	"fmt"
	"log"
	"os"

	spr "simulator/source/sprite"
	t "simulator/source/tile"
	u "simulator/source/utils"
	"strconv"
	"strings"
)

func FileToMap(filename string,
	wallSprites []*spr.Sprite,
	floorSprites []*spr.Sprite,
) *Map {
	folder := u.GetRootFolder("/maps/")
	dat, err := os.Open(folder + filename)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	fileScanner := bufio.NewScanner(dat)
	fileScanner.Split(bufio.ScanLines)

	fileMap := &Map{
		Cells: make([][]*Cell, 0),
		Cols:  0,
		Rows:  0,
	}

	line := 0
	for fileScanner.Scan() {
		if line == 0 {
			colRow := strings.Split(fileScanner.Text(), ",")

			cols, err := strconv.Atoi(colRow[0])
			if err != nil {
				log.Panicln("Map file malformed: ", fileScanner.Text(), " line ", line)
			}
			fileMap.Cols = int32(cols)

			rows, err := strconv.Atoi(colRow[1])
			if err != nil {
				log.Panicln("Map file malformed: ", fileScanner.Text(), " line ", line)
			}
			fileMap.Rows = int32(rows)

			fileMap.Cells = make([][]*Cell, fileMap.Cols)
			for i := range fileMap.Cells {
				fileMap.Cells[i] = make([]*Cell, fileMap.Rows)
				for j := range fileMap.Cells {
					fileMap.Cells[i][j] = &Cell{}
				}
			}
			line++
			continue
		}
		for j, val := range fileScanner.Text() {
			actualLine := line - 1
			switch val {
			default:
				fallthrough
			case '0':
				fileMap.Cells[actualLine][j].Tile = t.NewFloorTile(
					floorSprites,
					t.F_NO_SHADOW,
					0,
					u.Vector2[int32]{X: int32(actualLine), Y: int32(j)},
				)
			case '1':
				fileMap.Cells[actualLine][j].Tile = t.NewWallTile(
					wallSprites,
					t.W_ENCLOSED,
					false,
					u.Vector2[int32]{X: int32(actualLine), Y: int32(j)},
				)
			}
		}
		line++
	}

	return fileMap
}
