package gamemap

import (
	spr "simulator/source/sprite"
	t "simulator/source/tile"
	u "simulator/source/utils"

	"github.com/veandco/go-sdl2/sdl"
)

type Map struct {
	Cells [][]*Cell `json:"tiles"`
	Cols  int32     `json:"cols"`
	Rows  int32     `json:"rows"`
}

type Cell struct {
	Tile   t.Tileable
	Entity Entitier
}

type Entitier interface {
	Render(sdlRenderer *sdl.Renderer, offset *u.Vector2[int32])
	Move(gameMap *Map, delta float64)
}

func GenerateFilledMap(gamemap *Map, wallSprites []*spr.Sprite) {
	for i := int32(0); i < gamemap.Rows; i++ {
		gamemap.Cells[i] = make([]*Cell, gamemap.Rows)
		for j := int32(0); j < gamemap.Cols; j++ {
			gamemap.Cells[i][j] = &Cell{}
			gamemap.Cells[i][j].Tile = t.NewWallTile(
				wallSprites,
				t.W_ENCLOSED,
				false,
				u.Vector2[int32]{X: i, Y: j},
			)
		}
	}
}

func GenerateTestTileMap(
	gamemap *Map,
	wallSprites []*spr.Sprite,
	floorSprites []*spr.Sprite,
	num uint8,
	center uint8,
) {
	testMap := t.NumberToMap(num, center)
	gamemap.Rows = int32(len(testMap))
	gamemap.Cols = int32(len(testMap[0]))
	gamemap.Cells = make([][]*Cell, gamemap.Cols)
	for i := int32(0); i < gamemap.Rows; i++ {
		gamemap.Cells[i] = make([]*Cell, gamemap.Rows)
		for j := int32(0); j < gamemap.Cols; j++ {
			gamemap.Cells[i][j] = &Cell{}
			var tile t.Tileable
			tile = t.NewWallTile(
				wallSprites,
				t.W_ENCLOSED,
				false,
				u.Vector2[int32]{X: i, Y: j},
			)
			if testMap[i][j] == 0 {
				tile = t.NewFloorTile(
					floorSprites,
					t.F_NO_SHADOW,
					0,
					u.Vector2[int32]{X: int32(i), Y: int32(j)},
				)
			}
			gamemap.Cells[i][j].Tile = tile
		}
	}
}

func GetWallTileSpriteIndex(gamemap *Map, i, j int32) t.WallTileEnum {
	num := uint8(0)
	for k := 0; k < len(t.DIRECTIONS); k++ {
		posX := i + t.DIRECTIONS[k].X
		posY := j + t.DIRECTIONS[k].Y
		valid := u.CheckBound(posX, 0, gamemap.Rows) && u.CheckBound(posY, 0, gamemap.Cols)
		if valid {
			cell := gamemap.Cells[posX][posY]
			if cell == nil {
				continue
			}
			tile := cell.Tile
			if tile == nil {
				continue
			}
			_, ok := tile.(*t.WallTile)
			if !ok {
				continue
			}
		}
		num |= t.BITS[k]
	}
	return t.AUTOTILE_WALL[num]
}

func GetFloorTileShadowIndex(gamemap *Map, i, j int32) t.FloorTileShadowEnum {
	num := uint8(0)
	for k := 0; k < len(t.DIRECTIONS); k++ {
		posX := i + t.DIRECTIONS[k].X
		posY := j + t.DIRECTIONS[k].Y
		valid := u.CheckBound(posX, 0, gamemap.Rows) && u.CheckBound(posY, 0, gamemap.Cols)
		if valid {
			cell := gamemap.Cells[posX][posY]
			if cell == nil {
				continue
			}
			tile := cell.Tile
			if tile == nil {
				continue
			}
			_, ok := tile.(*t.WallTile)
			if !ok {
				continue
			}
		}
		num |= t.BITS[k]
	}
	return t.AUTOTILE_FLOOR[num]
}

type InHitboxable interface {
	InHitbox(pos *u.Vector2[int32]) bool
}

func ClickedMap(
	gamemap *Map,
	mousePos *u.Vector2[int32],
	sprites []*spr.Sprite,
	addFloor bool,
) bool {
	//TODO: optimize this
	for i := int32(0); i < gamemap.Rows; i++ {
		for j := int32(0); j < gamemap.Cols; j++ {
			cell := gamemap.Cells[i][j]
			if cell == nil {
				continue
			}
			tile := cell.Tile
			if tile == nil {
				continue
			}
			wallOrFloor, ok := tile.(InHitboxable)
			if !ok {
				continue
			}
			if wallOrFloor.InHitbox(mousePos) {
				gamemap.Cells[i][j].Tile = createWallOrFloor(i, j, gamemap, sprites, addFloor)
				AutotileAroundTile(gamemap, i, j)
				return true
			}
		}
	}
	return false
}

func createWallOrFloor(i int32, j int32, gamemap *Map, sprites []*spr.Sprite, floor bool) t.Tileable {
	if floor {
		floorTile := t.NewFloorTile(
			sprites,
			t.F_NO_SHADOW,
			0,
			u.Vector2[int32]{X: int32(i), Y: int32(j)},
		)
		floorTile.ShadowIndex = GetFloorTileShadowIndex(gamemap, i, j)
		return floorTile
	} else {
		wallTile := t.NewWallTile(
			sprites,
			t.W_ENCLOSED,
			false,
			u.Vector2[int32]{X: int32(i), Y: int32(j)},
		)
		wallTile.SpriteIndex = GetWallTileSpriteIndex(gamemap, i, j)
		return wallTile
	}
}

func AutotileAroundTile(gamemap *Map, i, j int32) {
	for k := 0; k < len(t.DIRECTIONS); k++ {
		posX := i + t.DIRECTIONS[k].X
		posY := j + t.DIRECTIONS[k].Y
		valid := u.CheckBound(posX, 0, gamemap.Rows) && u.CheckBound(posY, 0, gamemap.Cols)
		if valid {
			UpdateWallOrFloorSprite(gamemap, posX, posY)
		}
	}
}

func AutotileEntireMap(gamemap *Map) {
	for i := int32(0); i < gamemap.Cols; i++ {
		for j := int32(0); j < gamemap.Rows; j++ {
			UpdateWallOrFloorSprite(gamemap, i, j)

		}
	}
}

func UpdateWallOrFloorSprite(gamemap *Map, i, j int32) {
	cell := gamemap.Cells[i][j]
	if cell == nil {
		return
	}
	tile := cell.Tile
	if tile == nil {
		return
	}
	wall, ok := tile.(*t.WallTile)
	if ok {
		wall.SpriteIndex = GetWallTileSpriteIndex(gamemap, i, j)
		return
	}
	floor, ok := tile.(*t.FloorTile)
	if ok {
		floor.ShadowIndex = GetFloorTileShadowIndex(gamemap, i, j)
		return
	}
}
