package game

import (
	"log"
	m "simulator/source/gamemap"
	"simulator/source/gui"
	in "simulator/source/input"
	"simulator/source/sim"
	spr "simulator/source/sprite"
	u "simulator/source/utils"

	"github.com/veandco/go-sdl2/sdl"
)

const (
	SPRITE_WIDTH  = int32(16)
	SPRITE_HEIGHT = int32(16)
	SPRITE_SCALE  = int32(2)
)

type Game struct {
	FloorSprites      [][]*spr.Sprite `json:"floorSprites"`
	WallSprites       [][]*spr.Sprite `json:"wallSprites"`
	EntitySprites     []*spr.Sprite   `json:"entitySprites"`
	Map               *m.Map          `json:"map"`
	Entities          []*sim.Entity   `json:"entities"`
	State             GameStateEnum   `json:"state"`
	Camera            *Camera         `json:"camera"`
	Gui               *gui.Gui        `json:"gui"`
	CurrentWallBrush  int
	CurrentFloorBrush int
}

func NewGame(width, height int32) *Game {
	return &Game{
		FloorSprites:  make([][]*spr.Sprite, 0),
		WallSprites:   make([][]*spr.Sprite, 0),
		EntitySprites: make([]*spr.Sprite, 0),
		Map: &m.Map{
			Cells: make([][]*m.Cell, width),
			Cols:  int32(width),
			Rows:  int32(height),
		},
		State:             INITIALIZATION,
		CurrentWallBrush:  0,
		CurrentFloorBrush: 4,
	}
}

func (game *Game) InitializeGame(textures map[string]*sdl.Texture) {
	game.InitializeSprites(textures)
	loadedMap := m.FileToMap("map01", game.WallSprites[0], game.FloorSprites[4])
	if loadedMap == nil {
		m.GenerateFilledMap(game.Map, game.WallSprites[0])
	} else {
		game.Map = loadedMap
	}
	m.AutotileEntireMap(game.Map)
	entity := sim.NewEntity(
		2,
		game.EntitySprites,
		sim.HUMAN_MINER,
		&u.Vector2[int32]{X: 1, Y: 1},
	)
	game.Map.Cells[1][1].Entity = entity
	game.Entities = make([]*sim.Entity, 0)
	game.Entities = append(game.Entities, entity)
}

func (game *Game) InitializeSprites(textures map[string]*sdl.Texture) {
	game.FloorSprites = append(game.FloorSprites, GetSprites(textures, "floor_special_0"))
	game.FloorSprites = append(game.FloorSprites, GetSprites(textures, "floor_special_1"))
	game.FloorSprites = append(game.FloorSprites, GetSprites(textures, "floor_special_2"))
	game.FloorSprites = append(game.FloorSprites, GetSprites(textures, "floor_special_3"))
	game.FloorSprites = append(game.FloorSprites, GetSprites(textures, "floor0"))
	game.FloorSprites = append(game.FloorSprites, GetSprites(textures, "floor1"))
	game.FloorSprites = append(game.FloorSprites, GetSprites(textures, "floor2"))
	game.FloorSprites = append(game.FloorSprites, GetSprites(textures, "floor3"))
	game.FloorSprites = append(game.FloorSprites, GetSprites(textures, "floor4"))
	game.FloorSprites = append(game.FloorSprites, GetSprites(textures, "floor5"))
	game.WallSprites = append(game.WallSprites, GetSprites(textures, "wall0"))
	game.WallSprites = append(game.WallSprites, GetSprites(textures, "wall1"))
	game.WallSprites = append(game.WallSprites, GetSprites(textures, "wall2"))
	game.WallSprites = append(game.WallSprites, GetSprites(textures, "wall3"))
	game.WallSprites = append(game.WallSprites, GetSprites(textures, "wall4"))
	game.WallSprites = append(game.WallSprites, GetSprites(textures, "wall5"))
	game.EntitySprites = GetSprites(textures, "Player0")
}

func GetSprites(textures map[string]*sdl.Texture, name string) []*spr.Sprite {
	return spr.ExtractSpritesFromSpritesheet(textures[name], name, SPRITE_WIDTH, SPRITE_HEIGHT, SPRITE_SCALE)
}

func (game *Game) UpdateObjectsPositions(mousePos u.Vector2[int32]) {

}

func (game *Game) HandleClicking(mousePos *u.Vector2[int32], sprites []*spr.Sprite, addFloor bool) bool {
	relativeMousePos := &u.Vector2[int32]{
		X: mousePos.X - game.Camera.Pos.X,
		Y: mousePos.Y - game.Camera.Pos.Y,
	}
	return m.ClickedMap(game.Map, relativeMousePos, sprites, addFloor)
}

func (game *Game) IsGameEnded() bool {
	return false
}

// var NUM = uint8(0)
var NUM = 0

func (game *Game) HandleInput(input *in.GameInput, delta float64) GameStateEnum {
	if in.Input.Actions[in.SAVE] {
		log.Println("SAVED GAME")
		GameToFile(game, "savedstate")
	}
	if in.Input.Actions[in.LOAD] {
		log.Println("LOADED GAME")
		*game = *FileToGame("savedstate")
	}
	if in.Input.Actions[in.RESET] {
		/*GenerateTestTileMap(&game.Map, game.Walls[0], game.Floors[4], NUM, 1)
		GetFloorTileShadowIndex(&game.Map, 2, 2)
		AutotileEntireMap(&game.Map)
		*/
		//WaveFunctionCollapse(&game.Map, game.WallSprites[0], game.FloorSprites[4])
		NUM++
	}
	game.Camera.HandleInput(input, delta)

	if in.Input.ClickedAnyMouseButton() { //intercepts any clicking done on interface first
		if game.Gui != nil && game.Gui.HandleClicking(in.Input.MousePos) {
			return SIMULATE
		}
	}

	pressedNumRow, key1 := in.Input.PressedNumberRow(game.CurrentWallBrush)
	pressedNumRowMod, key2 := in.Input.PressedNumberRowWithModifier(game.CurrentFloorBrush)
	if pressedNumRowMod {
		game.CurrentFloorBrush = u.ClampMax(int(key2), len(game.FloorSprites)-1)
	} else if pressedNumRow {
		game.CurrentWallBrush = u.ClampMax(int(key1), len(game.WallSprites)-1)
	}
	if in.Input.Actions[in.MOUSE_LEFT_HOLD] {
		game.HandleClicking(in.Input.MousePos, game.FloorSprites[game.CurrentFloorBrush], true)
	} else if in.Input.Actions[in.MOUSE_RIGHT_HOLD] {
		game.HandleClicking(in.Input.MousePos, game.WallSprites[game.CurrentWallBrush], false)
	}
	return SIMULATE
}
