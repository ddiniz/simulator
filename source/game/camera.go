package game

import (
	in "simulator/source/input"
	u "simulator/source/utils"
)

type Camera struct {
	Pos      u.Vector2[int32]
	StepSize float64
}

func NewCamera() *Camera {
	return &Camera{
		Pos:      u.Vector2[int32]{X: 0, Y: 0},
		StepSize: 1000,
	}
}

func (camera *Camera) HandleInput(input *in.GameInput, delta float64) {
	if input.Actions[in.LEFT_RELEASED] || input.Actions[in.LEFT_HOLD] {
		camera.Pos.X += int32(camera.StepSize * delta)
	}
	if input.Actions[in.UP_RELEASED] || input.Actions[in.UP_HOLD] {
		camera.Pos.Y += int32(camera.StepSize * delta)
	}
	if input.Actions[in.RIGHT_RELEASED] || input.Actions[in.RIGHT_HOLD] {
		camera.Pos.X -= int32(camera.StepSize * delta)
	}
	if input.Actions[in.DOWN_RELEASED] || input.Actions[in.DOWN_HOLD] {
		camera.Pos.Y -= int32(camera.StepSize * delta)
	}
}
