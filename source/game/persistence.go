package game

import (
	"encoding/json"
	"fmt"
	"os"
	u "simulator/source/utils"
)

func GameToFile(game *Game, filename string) {
	folder := u.GetRootFolder("/")
	readFile, err := os.Create(folder + filename + ".json")
	if err != nil {
		fmt.Println(err)
		return
	}
	json, err := json.Marshal(game)
	if err != nil {
		fmt.Println(err)
		return
	}
	readFile.WriteString(string(json))
	readFile.Close()
}

func FileToGame(filename string) *Game {
	folder := u.GetRootFolder("/")
	dat, err := os.ReadFile(folder + filename)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	game := &Game{}
	json.Unmarshal(dat, game)
	return game
}
