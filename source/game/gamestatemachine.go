package game

import u "simulator/source/utils"

type GameStateEnum uint8

const (
	INITIALIZATION GameStateEnum = iota
	INPUT
	SIMULATE
	END
	RESTART
)

func (game *Game) ChangeState(next GameStateEnum) {
	switch game.State {
	case INITIALIZATION:
		switch next {
		case INPUT:
			game.State = INPUT
		default:
			u.PrintBadState(game.State, next)
		}
	case INPUT:
		switch next {
		case SIMULATE:
			game.State = SIMULATE
		case RESTART:
			game.State = RESTART
		default:
			u.PrintBadState(game.State, next)
		}
	case SIMULATE:
		switch next {
		case INPUT:
			game.State = INPUT
		case RESTART:
			game.State = RESTART
		default:
			u.PrintBadState(game.State, next)
		}
	case END:
		switch next {
		case END:
			game.State = END
		default:
			u.PrintBadState(game.State, next)
		}
	case RESTART:
		switch next {
		case INITIALIZATION:
			game.State = INITIALIZATION
		default:
			u.PrintBadState(game.State, next)
		}
	default:
		u.PrintBadState(game.State, next)
	}
}
