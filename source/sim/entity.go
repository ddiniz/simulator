package sim

import (
	"math/rand"
	m "simulator/source/gamemap"
	h "simulator/source/hitbox"
	spr "simulator/source/sprite"
	u "simulator/source/utils"

	"github.com/veandco/go-sdl2/sdl"
)

type EntitySpriteIndexEnum int32

const (
	NONE        EntitySpriteIndexEnum = iota
	HUMAN_MINER                       = 18
	UNKNOWN                           = 2
)

type Entity struct {
	Speed       int                   `json:"speed"`
	Wait        float64               `json:"wait"`
	Sprite      *spr.Sprite           `json:"sprite"`
	SpriteIndex EntitySpriteIndexEnum `json:"spriteIndex"`
	Hitbox      *h.Hitbox             `json:"hitbox"`
	Position    *u.Vector2[int32]     `json:"position"`
	MapPosition *u.Vector2[int32]     `json:"mapPosition"`

	Direction *u.Vector2[int32]
}

func NewEntity(
	speed int,
	sprite []*spr.Sprite,
	spriteIndex EntitySpriteIndexEnum,
	mapPosition *u.Vector2[int32],
) *Entity {
	entity := &Entity{
		Speed:       speed,
		Wait:        0,
		Sprite:      sprite[spriteIndex],
		SpriteIndex: NONE,
		Hitbox:      &h.Hitbox{X: 0, Y: 0, W: 0, H: 0},
		Position:    &u.Vector2[int32]{X: 0, Y: 0},
		MapPosition: mapPosition,
	}
	if len(sprite) > 0 {
		updateEntityPositionAndHitbox(entity, mapPosition.X, mapPosition.Y)
	}
	return entity
}

func updateEntityPositionAndHitbox(entity *Entity, newMapPosX int32, newMapPosY int32) {
	width := entity.Sprite.Scale * entity.Sprite.TextureRect.W
	height := entity.Sprite.Scale * entity.Sprite.TextureRect.H
	entity.Position = &u.Vector2[int32]{
		X: width * newMapPosX,
		Y: height * newMapPosY,
	}
	entity.Hitbox = &h.Hitbox{
		X: entity.Position.X,
		Y: entity.Position.Y,
		W: width,
		H: height,
	}
}

func (ent *Entity) Render(sdlRenderer *sdl.Renderer, offset *u.Vector2[int32]) {
	ent.Sprite.RenderAt(sdlRenderer, ent.Position, offset)
}

func (ent *Entity) Move(gameMap *m.Map, delta float64) {
	if ent.Wait <= 0 {
		ent.Wait = float64(ent.Speed)
		newPosX := ent.MapPosition.X + directions[0].X
		newPosY := ent.MapPosition.Y + directions[0].Y
		boundedMove := u.CheckBound(newPosX, 0, gameMap.Rows) && u.CheckBound(newPosY, 0, gameMap.Cols)
		if boundedMove && !gameMap.Cells[newPosX][newPosY].Tile.IsWall() {
			gameMap.Cells[newPosX][newPosY].Entity = gameMap.Cells[ent.MapPosition.X][ent.MapPosition.Y].Entity
			gameMap.Cells[ent.MapPosition.X][ent.MapPosition.Y].Entity = nil
			ent.MapPosition.X = newPosX
			ent.MapPosition.Y = newPosY
			updateEntityPositionAndHitbox(ent, newPosX, newPosY)
			return
		}
		chooseNewDirection()
		return
	}
	ent.Wait -= 100 * delta

}

func chooseNewDirection() {
	rand.Shuffle(len(directions), func(i, j int) { directions[i], directions[j] = directions[j], directions[i] })
}

var directions = []u.Vector2[int32]{
	//cardinals
	{X: -1, Y: +0},
	{X: +1, Y: +0},
	{X: +0, Y: -1},
	{X: +0, Y: +1},
	//diagonals
	{X: -1, Y: -1},
	{X: +1, Y: +1},
	{X: +1, Y: -1},
	{X: -1, Y: +1},
}
