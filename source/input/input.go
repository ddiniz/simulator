package input

import (
	u "simulator/source/utils"
)

type InputEnum uint16

const (
	MOUSE_LEFT_PRESSED InputEnum = iota
	MOUSE_MIDDLE_PRESSED
	MOUSE_RIGHT_PRESSED
	MOUSE_LEFT_HOLD
	MOUSE_MIDDLE_HOLD
	MOUSE_RIGHT_HOLD
	MOUSE_LEFT_RELEASED
	MOUSE_MIDDLE_RELEASED
	MOUSE_RIGHT_RELEASED

	LEFT_PRESSED
	RIGHT_PRESSED
	UP_PRESSED
	DOWN_PRESSED
	LEFT_HOLD
	RIGHT_HOLD
	UP_HOLD
	DOWN_HOLD
	LEFT_RELEASED
	RIGHT_RELEASED
	UP_RELEASED
	DOWN_RELEASED

	KEY_1
	KEY_2
	KEY_3
	KEY_4
	KEY_5
	KEY_6
	KEY_7
	KEY_8
	KEY_9
	KEY_0

	LCTRL

	QUIT
	RESET
	SAVE
	LOAD
	INVALID
)

type GameInput struct {
	MousePos *u.Vector2[int32]
	Actions  map[InputEnum]bool
}

var Input = &GameInput{
	MousePos: &u.Vector2[int32]{X: 0, Y: 0},
	Actions:  make(map[InputEnum]bool, 255),
}

var keys = []InputEnum{
	KEY_1,
	KEY_2,
	KEY_3,
	KEY_4,
	KEY_5,
	KEY_6,
	KEY_7,
	KEY_8,
	KEY_9,
	KEY_0,
}

func (in *GameInput) PressedNumberRow(current int) (bool, int) {
	for i := 0; i < len(keys); i++ {
		if in.Actions[keys[i]] {
			return true, i
		}
	}
	return false, current
}

func (in *GameInput) PressedNumberRowWithModifier(current int) (bool, int) {
	if !in.Actions[LCTRL] {
		return false, current
	}
	for i := 0; i < len(keys); i++ {
		if in.Actions[keys[i]] {
			return true, i
		}
	}
	return false, current
}

func (i *GameInput) ClickedAnyMouseButton() bool {
	return i.Actions[MOUSE_LEFT_PRESSED] ||
		i.Actions[MOUSE_MIDDLE_PRESSED] ||
		i.Actions[MOUSE_RIGHT_PRESSED] ||
		i.Actions[MOUSE_LEFT_HOLD] ||
		i.Actions[MOUSE_MIDDLE_HOLD] ||
		i.Actions[MOUSE_RIGHT_HOLD] ||
		i.Actions[MOUSE_LEFT_RELEASED] ||
		i.Actions[MOUSE_MIDDLE_RELEASED] ||
		i.Actions[MOUSE_RIGHT_RELEASED]
}

func (input *GameInput) ResetInput() {
	for i := range input.Actions {
		if i == MOUSE_LEFT_HOLD ||
			i == MOUSE_MIDDLE_HOLD ||
			i == MOUSE_RIGHT_HOLD ||
			i == UP_HOLD ||
			i == RIGHT_HOLD ||
			i == LEFT_HOLD ||
			i == DOWN_HOLD {
			continue
		}
		input.Actions[i] = false
	}
}
