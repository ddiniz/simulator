package tile

import (
	u "simulator/source/utils"

	"github.com/veandco/go-sdl2/sdl"
)

type Tileable interface {
	Render(sdlRenderer *sdl.Renderer, offset *u.Vector2[int32])
	InHitbox(pos *u.Vector2[int32]) bool
	IsWall() bool
}

// 0,1,2
// 3, ,4
// 5,6,7
var DIRECTIONS = []u.Vector2[int32]{
	{X: -1, Y: -1},
	{X: 0, Y: -1},
	{X: +1, Y: -1},
	{X: -1, Y: 0},
	{X: +1, Y: 0},
	{X: -1, Y: +1},
	{X: 0, Y: +1},
	{X: +1, Y: +1},
}

var BITS = []uint8{
	1, 2, 4,
	8 /**/, 16,
	32, 64, 128,
}

func NumberToMap(num, _c uint8) [][]uint8 {
	simpleMap := make([][]uint8, 5)
	nw := (num & 1) / 1
	_n := (num & 2) / 2
	ne := (num & 4) / 4
	_w := (num & 8) / 8
	_e := (num & 16) / 16
	sw := (num & 32) / 32
	_s := (num & 64) / 64
	se := (num & 128) / 128
	simpleMap[0] = append(simpleMap[0], []uint8{1, 1, 1, 1, 1}...)
	simpleMap[1] = append(simpleMap[1], []uint8{1, nw, _w, sw, 1}...)
	simpleMap[2] = append(simpleMap[2], []uint8{1, _n, _c, _s, 1}...)
	simpleMap[3] = append(simpleMap[3], []uint8{1, ne, _e, se, 1}...)
	simpleMap[4] = append(simpleMap[4], []uint8{1, 1, 1, 1, 1}...)
	return simpleMap
}
