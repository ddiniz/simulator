package tile

import (
	h "simulator/source/hitbox"
	spr "simulator/source/sprite"
	u "simulator/source/utils"

	"github.com/veandco/go-sdl2/sdl"
)

type WallTileEnum uint8

const (
	W_CORNER_TOP_LEFT WallTileEnum = iota
	W_LEFT
	W_CORNER_BOTTOM_LEFT
	W_HORIZONTAL_LEFT

	W_TOP
	W_ENCLOSED
	W_BOTTOM
	W_HORIZONTAL

	W_CORNER_TOP_RIGHT
	W_RIGHT
	W_CORNER_BOTTOM_RIGHT
	W_HORIZONTAL_RIGHT

	W_VERTICAL_TOP
	W_VERTICAL
	W_VERTICAL_BOTTOM
	W_PILLAR

	W_VERTICAL_TOP_SHADOW
	W_VERTICAL_SHADOW
	W_VERTICAL_BOTTOM_SHADOW
	W_PILLAR_SHADOW

	W_LINE_RIGHT_BOTTOM
	W_LINE_TOP_RIGHT_BOTTOM
	W_LINE_TOP_RIGHT
	W_HORIZONTAL_LEFT_SHADOW

	W_LINE_LEFT_BOTTOM_RIGHT
	W_LINE_CROSS
	W_LINE_LEFT_TOP_RIGHT
	W_HORIZONTAL_SHADOW

	W_LINE_LEFT_BOTTOM
	W_LINE_TOP_LEFT_BOTTOM
	W_LINE_LEFT_TOP
	W_HORIZONTAL_RIGHT_SHADOW

	W_p
	W_OPEN_LEFT_TOP_RIGHT_BOTTOM
	W_b
	W_p_SHADOW

	W_OPEN_TOP_LEFT_BOTTOM_RIGHT
	W_EMPTY0
	W_OPEN_BOTTOM_LEFT_TOP_RIGHT
	W_LINE_TOP_RIGHT_BOTTOM_SHADOW

	W_q
	W_OPEN_RIGHT_LEFT_TOP_BOTTOM
	W_d
	W_b_SHADOW

	W_LINE_CROSS_BOTTOM_RIGHT
	W_LINE_CROSS_TOP_RIGHT
	W_CORNER_TOP_LEFT_SHADOW
	W_LEFT_SHADOW

	W_LINE_CROSS_BOTTOM_LEFT
	W_LINE_CROSS_TOP_LEFT
	W_CORNER_BOTTOM_LEFT_SHADOW
	W_SPECIAL_TOP_LEFT_SHADOW

	W_SPECIAL_BOTTOM_LEFT
	W_SPECIAL_TOP_LEFT
	W_BOTTOM_SHADOW
	W_SPECIAL_TOP_RIGHT_SHADOW

	W_SPECIAL_BOTTOM_RIGHT
	W_SPECIAL_TOP_RIGHT
	W_CORNER_BOTTOM_RIGHT_SHADOW
	W_LINE_LEFT_TOP_RIGHT_SHADOW

	W_INNER_CORNER_BOTTOM_RIGHT
	W_INNER_CORNER_TOP_RIGHT
	W_LINE_RIGHT_BOTTOM_SHADOW
	W_LINE_TOP_RIGHT_SHADOW

	W_INNER_CORNER_BOTTOM_LEFT
	W_INNER_CORNER_TOP_LEFT
	W_EMPTY1
	W_LINE_LEFT_TOP_SHADOW

	W_SLASH
	W_BACKSLASH
	W_EMPTY2
	W_EMPTY3
)

type WallTile struct {
	Sprite      []*spr.Sprite    `json:"sprite"`
	SpriteIndex WallTileEnum     `json:"spriteIndex"`
	Breakable   bool             `json:"breakable"`
	Hitbox      *h.Hitbox        `json:"hitbox"`
	Position    u.Vector2[int32] `json:"position"`
}

func NewWallTile(
	sprite []*spr.Sprite,
	spriteIndex WallTileEnum,
	breakable bool,
	position u.Vector2[int32],
) *WallTile {
	wall := &WallTile{
		Sprite:      sprite,
		SpriteIndex: spriteIndex,
		Breakable:   breakable,
	}
	if len(sprite) > 0 {
		width := sprite[0].Scale * sprite[0].TextureRect.W
		height := sprite[0].Scale * sprite[0].TextureRect.H
		wall.Position = u.Vector2[int32]{
			X: width * position.X,
			Y: height * position.Y,
		}
		wall.Hitbox = &h.Hitbox{
			X: wall.Position.X,
			Y: wall.Position.Y,
			W: width,
			H: height,
		}
	}
	return wall
}

func (tile *WallTile) Render(sdlRenderer *sdl.Renderer, offset *u.Vector2[int32]) {
	tile.Sprite[tile.SpriteIndex].RenderAt(sdlRenderer, &tile.Position, offset)
}

func (tile *WallTile) InHitbox(pos *u.Vector2[int32]) bool {
	return tile.Hitbox.InHitbox(pos)
}

func (tile *WallTile) IsWall() bool {
	return true
}
