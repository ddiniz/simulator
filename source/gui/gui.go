package gui

import (
	u "simulator/source/utils"

	"github.com/veandco/go-sdl2/sdl"
)

type Gui struct {
	Pos    *u.Vector2[int32]
	Dim    *u.Vector2[int32]
	Rect   *u.Rect[int32]
	Color  *u.Color[uint8]
	Hidden bool
	ZIndex int8
}

func NewGUI(pos *u.Vector2[int32], dim *u.Vector2[int32], color *u.Color[uint8]) *Gui {
	gui := &Gui{
		Dim:    pos,
		Pos:    dim,
		Color:  color,
		ZIndex: 1,
		Hidden: false,
	}
	gui.Rect = GetRealPositionsAndDimensions(gui.Pos, gui.Dim)
	return gui
}

type TextGui struct {
	Gui
	Rect   *u.Rect[int32]
	Offset *u.Vector2[int32]
	Text   string
	ZIndex int8
}

type ButtonGui struct {
	Gui
	Rect   *u.Rect[int32]
	Offset *u.Vector2[int32]
	Text   string
	ZIndex int8
}

func inArea(pos *u.Vector2[int32], area *u.Rect[int32]) bool {
	return u.CheckBound(pos.X, area.X, area.W) && u.CheckBound(pos.Y, area.Y, area.H)
}

func GetRealPositionsAndDimensions(pos *u.Vector2[int32], rect *u.Vector2[int32]) *u.Rect[int32] {
	return &u.Rect[int32]{
		X: pos.X,
		Y: pos.Y,
		W: pos.X + rect.X,
		H: pos.Y + rect.Y,
	}
}

func (gui *Gui) HandleClicking(pos *u.Vector2[int32]) bool {
	clicked := inArea(pos, gui.Rect)
	if clicked {
		gui.Action()
	}
	return clicked
}

func (gui *Gui) Action() {

}

func (gui *Gui) Render(sdlRenderer *sdl.Renderer) {
	if gui.Hidden {
		return
	}
	sdlRenderer.SetDrawColor(gui.Color.R, gui.Color.G, gui.Color.B, gui.Color.A)
	sdlRenderer.FillRect(&sdl.Rect{X: gui.Rect.X, Y: gui.Rect.Y, W: gui.Rect.W, H: gui.Rect.H})
}

func (textGui *TextGui) HandleClicking() bool {
	return false
}

func (textGui *TextGui) Action() {

}

func (textGui *TextGui) Render(sdlRenderer *sdl.Renderer) {

}

func (buttonGui *ButtonGui) HandleClicking() bool {
	return false
}

func (buttonGui *ButtonGui) Action() {

}

func (buttonGui *ButtonGui) Render(sdlRenderer *sdl.Renderer) {

}
