package main

import (
	"math/rand"
	mysdl "simulator/source/sdl"
	"time"
)

//TODO: implement some type of pallete function to change tile colors
//TODO: GUI
//TODO: Main menu
//TODO: Map builder menu and interface
//TODO: save and load maps
//TODO: A* algorithm
//TODO: A* algorithm that responds to map modifications

func main() {
	rand.Seed(time.Now().UnixNano())
	mysdl.RunGame()
}
