package utils

import (
	"encoding/json"
	"log"
	"os"
	"path/filepath"
)

func SaveStruct[T any](myStruct T, folderPath string, filepath string, pretty bool) {
	folder := GetRootFolder("/" + folderPath + "/")
	readFile, err := os.Create(folder + filepath)
	if err != nil {
		log.Println(err)
	}
	var res []byte
	if pretty {
		res, err = json.MarshalIndent(myStruct, "", "\t")
	} else {
		res, err = json.Marshal(myStruct)
	}

	if err != nil {
		log.Println(err)
	}
	readFile.WriteString(string(res))
	readFile.Close()
}

func LoadStruct[T any](defaultValues *T, folderPath string, filepath string) *T {
	folder := GetRootFolder("/" + folderPath + "/")
	dat, err := os.ReadFile(folder + filepath)
	if err != nil {
		log.Println(err)
		return defaultValues
	}
	var myStruct *T
	json.Unmarshal(dat, &myStruct)
	return myStruct
}

func GetRootFolder(path string) string {
	folder := GetExecutablePath() + path //This to find if debug used
	if _, err := os.Stat(folder); !os.IsNotExist(err) {
		return folder
	}

	folder = GetCurrentDir() + path //This to find if go run used
	if _, err := os.Stat(folder); !os.IsNotExist(err) {
		return folder
	}

	log.Fatal("Unable to locate save folder")
	return ""
}

func GetCurrentDir() string {
	path, err := os.Getwd()
	if err != nil {
		log.Fatalf("Unable to open current dir with err: %s", err)
	}
	return path
}

func GetExecutablePath() string {
	ex, err := os.Executable()
	if err != nil {
		log.Fatalf("Unable to locate executable current dir with err: %s", err)
	}
	return filepath.Dir(ex)
}
