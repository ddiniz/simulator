package utils

type Vector2[T any] struct {
	X T
	Y T
}

type Vector3[T any] struct {
	X T
	Y T
	Z T
}
type Color[T any] struct {
	R T
	G T
	B T
	A T
}

type Rect[T any] struct {
	X T
	Y T
	W T
	H T
}
