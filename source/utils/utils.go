package utils

import (
	"log"
)

func GetLast[T any](array []*T) *T {
	if len(array) > 0 {
		return array[len(array)-1]
	}
	return nil
}

func IsEmpty[T any](array []*T) bool {
	return len(array) == 0
}

func CheckBound[T int64 | int32 | int | float64 | float32](val, x, w T) bool {
	return x <= val && val < x+w
}

func PrintBadState(current any, next any) {
	log.Println("Bad game state, current:", current, " next:", next)
}

func ClampMax[T int64 | int32 | int | float64 | float32](val T, max T) T {
	if val > max {
		return max
	}
	return val
}
