package sprite

import (
	u "simulator/source/utils"

	"github.com/veandco/go-sdl2/sdl"
)

type Sprite struct {
	Texture     *sdl.Texture `json:"texture"`
	Scale       int32        `json:"scale"`
	Name        string       `json:"name"`
	TextureRect *sdl.Rect    `json:"textureRect"`
	PosRect     *sdl.Rect    `json:"posRect"`
}

func NewSprite(
	texture *sdl.Texture,
	name string,
	textureRect *sdl.Rect,
	position *sdl.Rect,
	scale int32,
) *Sprite {
	sprite := &Sprite{
		Texture:     texture,
		Scale:       scale,
		Name:        name,
		TextureRect: textureRect,
		PosRect:     position,
	}
	sprite.ScaleSprite()
	return sprite
}

func (sprite *Sprite) RenderAt(
	sdlRenderer *sdl.Renderer,
	pos *u.Vector2[int32],
	offset *u.Vector2[int32],
) {
	sdlRenderer.Copy(
		sprite.Texture,
		sprite.TextureRect,
		&sdl.Rect{
			X: pos.X + offset.X,
			Y: pos.Y + offset.Y,
			W: sprite.PosRect.W,
			H: sprite.PosRect.H,
		},
	)
}

func (sprite *Sprite) ScaleSprite() {
	sprite.PosRect.W = sprite.PosRect.W * sprite.Scale
	sprite.PosRect.H = sprite.PosRect.H * sprite.Scale
}

func ExtractSpritesFromSpritesheet(
	texture *sdl.Texture,
	name string,
	sprWidth int32,
	sprHeight int32,
	sprScale int32,
) []*Sprite {
	_, _, width, height, _ := texture.Query()
	cols := width / sprWidth
	rows := height / sprHeight
	sprites := make([]*Sprite, 0)
	for i := int32(0); i < cols; i++ {
		for j := int32(0); j < rows; j++ {
			sprites = append(
				sprites,
				NewSprite(
					texture,
					name,
					&sdl.Rect{
						X: i * sprWidth,
						Y: j * sprHeight,
						W: sprWidth,
						H: sprHeight,
					},
					&sdl.Rect{
						X: 0,
						Y: 0,
						W: sprWidth,
						H: sprHeight,
					},
					sprScale,
				),
			)
		}
	}
	return sprites
}
