package sdl

import (
	"log"
	g "simulator/source/game"
	in "simulator/source/input"
)

func GameLoop(game *g.Game, delta float64) {
	switch game.State {
	case g.INITIALIZATION:
		log.Println("INITIALIZATION")
		game.ChangeState(g.INPUT)
	case g.INPUT:
		newState := game.HandleInput(in.Input, delta)

		if newState != game.State {
			game.ChangeState(newState)
		}
	case g.SIMULATE:
		for _, ent := range game.Entities {
			ent.Move(game.Map, delta)
		}
		game.ChangeState(g.INPUT)
	case g.END:
		log.Println("END")
		game.ChangeState(g.RESTART)
	case g.RESTART:
		log.Println("RESTART")
		game.ChangeState(g.INITIALIZATION)
	}
}
