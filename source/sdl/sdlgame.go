package sdl

import (
	"fmt"
	"os"
	g "simulator/source/game"
	in "simulator/source/input"
	"time"

	"github.com/veandco/go-sdl2/sdl"
)

const (
	MAP_ROW_SIZE = int32(64)
	MAP_COL_SIZE = int32(64)
)

func RunGame() {
	camera := g.NewCamera()
	// gui := gui.NewGUI(
	// 	u.Vector2[int32]{X: 100, Y: 100},
	// 	u.Vector2[int32]{X: 32, Y: 32},
	// 	u.Color[uint8]{R: 0, G: 0, B: 0, A: 255},
	// )
	game := g.NewGame(MAP_ROW_SIZE, MAP_COL_SIZE)
	// windowWidth := game.Map.Cols * g.SPRITE_WIDTH * g.SPRITE_SCALE
	windowWidth := int32(1920)
	// windowHeight := game.Map.Rows * g.SPRITE_HEIGHT * g.SPRITE_SCALE
	windowHeight := int32(1080)
	window, sdlRenderer, _ := initializeSDL(
		"simulator",
		windowWidth,
		windowHeight,
	)
	defer sdl.Quit()
	defer window.Destroy()
	defer sdlRenderer.Destroy()
	images := loadImages()
	textures := loadTextures(images, sdlRenderer)
	defer unloadImages(images)
	defer unloadTextures(textures)

	sdl.JoystickEventState(sdl.ENABLE)
	game.InitializeGame(textures)
	game.Camera = camera
	//game.Gui = gui
	now := time.Now()
	for !in.Input.Actions[in.QUIT] {
		last := now
		now = time.Now()
		delta := now.Sub(last).Seconds()
		sdlRenderer.Clear()
		sdlRenderer.SetDrawColor(255, 255, 255, 255)
		sdlRenderer.FillRect(&sdl.Rect{X: 0, Y: 0, W: windowWidth, H: windowHeight})

		GetInput()
		GameLoop(game, delta)
		RenderGame(sdlRenderer, game, camera)
		//RenderGui(sdlRenderer, gui)

		sdlRenderer.Present()
		sdl.Delay(16)
	}
}

func initializeSDL(
	winTitle string,
	winWidth int32,
	winHeight int32,
) (*sdl.Window, *sdl.Renderer, error) {
	if err := sdl.Init(sdl.INIT_EVERYTHING); err != nil {
		panic(err)
	}
	window, err := sdl.CreateWindow(
		winTitle,
		sdl.WINDOWPOS_UNDEFINED,
		sdl.WINDOWPOS_UNDEFINED,
		winWidth,
		winHeight,
		sdl.WINDOW_SHOWN|sdl.WINDOW_MAXIMIZED|sdl.WINDOW_RESIZABLE,
	)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create window: %s\n", err)
		return nil, nil, err
	}

	renderer, err := sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create renderer: %s\n", err)
		return nil, nil, err
	}
	return window, renderer, err
}
