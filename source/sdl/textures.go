package sdl

import (
	"log"
	u "simulator/source/utils"

	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
)

const textureSrc = "/textures/"

var files = []string{
	"floor_special_0",
	"floor_special_1",
	"floor_special_2",
	"floor_special_3",
	"floor0",
	"floor1",
	"floor2",
	"floor3",
	"floor4",
	"floor5",
	"wall0",
	"wall1",
	"wall2",
	"wall3",
	"wall4",
	"wall5",
	"Player0",
}

func loadImages() map[string]*sdl.Surface {
	folder := u.GetRootFolder(textureSrc)

	images := make(map[string]*sdl.Surface)
	for _, file := range files {
		image, err := img.Load(folder + file + ".png")
		if err != nil {
			log.Fatalf("Failed to load PNG: %s\n", err)
		}
		images[file] = image
	}
	return images
}

func unloadImages(images map[string]*sdl.Surface) {
	for _, value := range images {
		value.Free()
	}
}

func loadTextures(images map[string]*sdl.Surface, renderer *sdl.Renderer) map[string]*sdl.Texture {
	textures := make(map[string]*sdl.Texture)
	for key, value := range images {
		texture, err := renderer.CreateTextureFromSurface(value)
		if err != nil {
			log.Fatalf("Failed to create texture: %s\n", err)
		}
		textures[key] = texture
	}
	return textures
}

func unloadTextures(textures map[string]*sdl.Texture) {
	for _, value := range textures {
		value.Destroy()
	}
}
