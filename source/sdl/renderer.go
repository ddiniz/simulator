package sdl

import (
	g "simulator/source/game"
	gui "simulator/source/gui"
	u "simulator/source/utils"

	"github.com/veandco/go-sdl2/sdl"
)

type Renderable interface {
	//Render(sdlRenderer *sdl.Renderer, offset u.Vector2[int32])
	Render(sdlRenderer *sdl.Renderer, offset *u.Vector2[int32])
}

func RenderGame(sdlRenderer *sdl.Renderer, game *g.Game, camera *g.Camera) {
	for _, mapRow := range game.Map.Cells {
		for _, cell := range mapRow {
			if cell == nil {
				continue
			}
			renderable := cell.Tile.(Renderable)
			if renderable != nil {
				renderable.Render(sdlRenderer, &camera.Pos)
			}
			if cell.Entity != nil {
				cell.Entity.Render(sdlRenderer, &camera.Pos)
			}
		}
	}

}

func RenderGui(sdlRenderer *sdl.Renderer, gui *gui.Gui) {
	gui.Render(sdlRenderer)
}
