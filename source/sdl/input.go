package sdl

import (
	in "simulator/source/input"

	"github.com/veandco/go-sdl2/sdl"
)

func GetInput() {
	in.Input.ResetInput()
	for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
		switch t := event.(type) {
		case *sdl.QuitEvent:
			in.Input.Actions[in.QUIT] = true
		case *sdl.MouseMotionEvent:
			in.Input.MousePos.X = t.X
			in.Input.MousePos.Y = t.Y
		case *sdl.MouseButtonEvent:
			if t.Type == sdl.MOUSEBUTTONDOWN {
				if t.Button == sdl.BUTTON_LEFT {
					in.Input.Actions[in.MOUSE_LEFT_PRESSED] = true
					in.Input.Actions[in.MOUSE_LEFT_HOLD] = true
				}
				if t.Button == sdl.BUTTON_MIDDLE {
					in.Input.Actions[in.MOUSE_MIDDLE_PRESSED] = true
					in.Input.Actions[in.MOUSE_MIDDLE_HOLD] = true
				}
				if t.Button == sdl.BUTTON_RIGHT {
					in.Input.Actions[in.MOUSE_RIGHT_PRESSED] = true
					in.Input.Actions[in.MOUSE_RIGHT_HOLD] = true
				}
			}
			if t.Type == sdl.MOUSEBUTTONUP {
				if t.Button == sdl.BUTTON_LEFT {
					in.Input.Actions[in.MOUSE_LEFT_RELEASED] = true
					in.Input.Actions[in.MOUSE_LEFT_HOLD] = false
				}
				if t.Button == sdl.BUTTON_MIDDLE {
					in.Input.Actions[in.MOUSE_MIDDLE_RELEASED] = true
					in.Input.Actions[in.MOUSE_MIDDLE_HOLD] = false
				}
				if t.Button == sdl.BUTTON_RIGHT {
					in.Input.Actions[in.MOUSE_RIGHT_RELEASED] = true
					in.Input.Actions[in.MOUSE_RIGHT_HOLD] = false
				}
			}
		case *sdl.KeyboardEvent:
			if t.Type == sdl.KEYDOWN {
				if t.Keysym.Scancode == sdl.SCANCODE_R {
					in.Input.Actions[in.RESET] = true
				} else if t.Keysym.Scancode == sdl.SCANCODE_ESCAPE {
					in.Input.Actions[in.QUIT] = true
				} else if t.Keysym.Scancode == sdl.SCANCODE_F5 {
					in.Input.Actions[in.SAVE] = true
				} else if t.Keysym.Scancode == sdl.SCANCODE_F6 {
					in.Input.Actions[in.LOAD] = true
				}

				//arrow keys
				if t.Keysym.Scancode == sdl.SCANCODE_LEFT {
					in.Input.Actions[in.LEFT_PRESSED] = true
					in.Input.Actions[in.LEFT_HOLD] = true
				}
				if t.Keysym.Scancode == sdl.SCANCODE_UP {
					in.Input.Actions[in.UP_PRESSED] = true
					in.Input.Actions[in.UP_HOLD] = true
				}
				if t.Keysym.Scancode == sdl.SCANCODE_RIGHT {
					in.Input.Actions[in.RIGHT_PRESSED] = true
					in.Input.Actions[in.RIGHT_HOLD] = true
				}
				if t.Keysym.Scancode == sdl.SCANCODE_DOWN {
					in.Input.Actions[in.DOWN_PRESSED] = true
					in.Input.Actions[in.DOWN_HOLD] = true
				}

				if t.Keysym.Mod == 4160 { // sdl.KMOD_LCTRL isnt the right value, maybe I'm dumb.
					in.Input.Actions[in.LCTRL] = true
				}

				for i := 0; i < len(scancodesSDL); i++ {
					if t.Keysym.Scancode == scancodesSDL[i] {
						key := in.InputEnum(i + int(in.KEY_1))
						in.Input.Actions[key] = true
					}
				}
			}

			if t.Type == sdl.KEYUP {
				//arrow keys
				if t.Keysym.Scancode == sdl.SCANCODE_LEFT {
					in.Input.Actions[in.LEFT_PRESSED] = true
					in.Input.Actions[in.LEFT_HOLD] = false
				}
				if t.Keysym.Scancode == sdl.SCANCODE_UP {
					in.Input.Actions[in.UP_PRESSED] = true
					in.Input.Actions[in.UP_HOLD] = false
				}
				if t.Keysym.Scancode == sdl.SCANCODE_RIGHT {
					in.Input.Actions[in.RIGHT_PRESSED] = true
					in.Input.Actions[in.RIGHT_HOLD] = false
				}
				if t.Keysym.Scancode == sdl.SCANCODE_DOWN {
					in.Input.Actions[in.DOWN_PRESSED] = true
					in.Input.Actions[in.DOWN_HOLD] = false
				}
			}
		}
	}
}

var scancodesSDL = []sdl.Scancode{
	sdl.SCANCODE_0,
	sdl.SCANCODE_1,
	sdl.SCANCODE_2,
	sdl.SCANCODE_3,
	sdl.SCANCODE_4,
	sdl.SCANCODE_5,
	sdl.SCANCODE_6,
	sdl.SCANCODE_7,
	sdl.SCANCODE_8,
	sdl.SCANCODE_9,
}
