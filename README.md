# simulator

Playing around with simulating stuff

This project is still in development


# Prerequisites

Golang 1.20.2

SDL2 and SDL2-image

# Running

```
go run -tags sdl source/cmd/app/main.go
```

# Build

```
go build -tags sdl source/cmd/app/main.go
```

# License

Do what you want with the code and assets, just give credit.

Code - Davi Diniz 

Assets - Ripped from from Gaunlet 4 and modified slightly or not
